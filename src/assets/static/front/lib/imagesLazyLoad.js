/* eslint semi:0 */

class LazyLoadImages {
  constructor() {
    this.selector = 'img[data-lazy-load=""]'
    this.reload = this.reload.bind(this)
    this.reload()
    this.alreadyLoaded = []
    this.alreadyLoaded.add = function(img) {
      this.push(img.dataset.x1)
    }

    if ('IntersectionObserver' in window) {
      let onObserve = this.makeOnEntriesObserve(this.alreadyLoaded)
      this.observer = new IntersectionObserver(onObserve)
      this.entities.map(img => this.observer.observe(img))
    } else {
      this.loadFirstImage(this.alreadyLoaded, this.entities)
      let onScrollHandle = this.makeOnScrollDebounced(this.alreadyLoaded)(this.entities)
      document.addEventListener('scroll', onScrollHandle)
    }
    this.loadImagesPreviews(this.entities)
  }

  reload() {
    let entitiesNodes = document.querySelectorAll(this.selector)
    let entities = Array.from(entitiesNodes)
    if (!this.entities || entities.length > this.entities.length) {
      this.entities = entities
      this.observer && this.entities.map(img => this.observer.observe(img))
    }
  }

  loadImagesPreviews(entities) {
    let onLoad = img =>
      new Promise(resolve => {
        if (!img.dataset.preview)
          return resolve()
        img.src = img.dataset.preview
        img.onload = resolve
      })

    let load = ([ x, ...xs ]) =>
      x && onLoad(x).then(() => load(xs))

    load(entities)
  }

  loadImage(img) {
    return new Promise(resolve => {
      let container = document.createElement('div')
      container.classList = img.classList
      container.style.cssText = window.getComputedStyle(img)
      container.style.position = 'relative'

      img.parentNode.replaceChild(container, img)
      container.appendChild(img)

      let newImg = img.cloneNode(false)
      // Manually check a resolution because media does not work with data attributes (no srcset on load)
      newImg.src = window.innerWidth >= 1280
        ? img.dataset.x2
        : img.dataset.x1

      newImg.onload = () => {
        img.style.position = 'absolute'
        img.style.top = 0
        img.style.left = 0
        img.style.transition = 'opacity .5s ease-out'
        img.style.opacity = 1

        img.parentNode.insertBefore(newImg, img)
        img.style.opacity = 0

        resolve()
      }
    })
  }
  
  makeOnEntriesObserve(alreadyLoaded) {
    return entities => {
      entities.forEach(entity => {
        let img = entity.target
        let preview = img.dataset.x1
        if (
          !alreadyLoaded.includes(preview) &&
          entity.isIntersecting
        ) {
          alreadyLoaded.add(img)
          this.loadImage(img)
        }
      })
    }
  }
  
  makeOnScrollDebounced(alreadyLoaded) {
    let debounce = fn => {
      let timeout
      return function() {
        timeout && window.cancelAnimationFrame(timeout)
        timeout = window.requestAnimationFrame(() => fn())
      }
    }

    return entities =>
      debounce(() => {
        entities.map(img => {
          let preview = img.dataset.x1
          if (this.alreadyLoaded.includes(preview)) return false
  
          let scrollTop = document.documentElement.scrollTop
          let top = img.getBoundingClientRect().top
  
          if (top <= scrollTop) {
            alreadyLoaded.push(preview)
            this.loadImage(img)
          }
        })
      })
  }

  loadFirstImage(alreadyLoaded, entities) {
    alreadyLoaded.add(entities[0])
    this.loadImage(entities[0])
  }
}

window.lazyLoadImages = new LazyLoadImages()
setTimeout(() => window.lazyLoadImages.reload(), 5000)

// document.addEventListener('DOMContentLoaded', () =>
//   window.lazyLoadImages.reload())
