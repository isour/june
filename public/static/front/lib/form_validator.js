window._ || (window._ = {
  isFunction: function (func) {
    return typeof func == 'function';
  },
  isObject: function (obj) {
    return obj === Object(obj);
  },
  isUndefined: function (value) {
    return value === void 0;
  }
});

var FormValidator = (function () {
  function FormValidator(selector) {
    this.form = $(selector);
    this.options = {
      rules: {},
      messages: {},
      errorPlacement: function (fieldName, message) {
      },
      successPlacement: function (fieldName) {
      },
      events: ['focusout'],
      onload: false,
      changeSubmitButtonClass: true
    };
    this.errors = {};
    this.isValid = false;
    this.defaultMessage = 'Поле заполнено неверно'
  }

  FormValidator.prototype.getInputField = function (fieldName) {
    return this.form.find('input[name="' + fieldName + '"]').eq(0);
  };

  FormValidator.prototype.validate = function (options) {
    this.setOptions(options);
    this.flushErrors();
    this.runValidation(!this.options.onload);
    this.bindEventsHandler();
    this.bindSubmitHandler();
  };

  FormValidator.prototype.setOptions = function (options) {
    this.options = $.extend(this.options, options);
  };

  FormValidator.prototype.flushErrors = function () {
    this.formWalk(function (input) {
      this.errors[input.name] = true;
    });
  };

  FormValidator.prototype.bindEventsHandler = function () {
    var self = this;
    this.formWalk(function (input) {
      $(input).on(this.options.events.join(' '), function (ev) {
        self.event = ev;
        self.validateField(ev);
      });
    });
  };

  FormValidator.prototype.runValidation = function (withoutRender) {
    var deferreds = [];
    this.isValid = false;
    this.formWalk(function (input) {
      var result = this.validateField(input, withoutRender);

      if ($.isArray(result)) {
        deferreds = deferreds.concat(result);
      }
    });
    return $.when.apply(null, deferreds); // return combined promise for all deferred object we've collected
  };

  FormValidator.prototype.formWalk = function (callable) {
    var $this = this;
    this.form.find('input[type="text"],input[type="password"],input[type="checkbox"],input[type="hidden"],textarea,select').each(function (i, input) {
      if (!_.isUndefined($this.options.rules[input.name]) && _.isFunction(callable)) {
        callable.call($this, input);
      }
    });
  };

  FormValidator.prototype.validateField = function (input, withoutRender) {
    if (input.target != undefined) {
      input = input.target;
      withoutRender = false;
    }
    var name = input.name,
      that = this,
      deferreds = [],
      finalCallback = this.getFinalValidationCallback(withoutRender);

    if (!_.isObject(this.options.rules[name])) {
      this.errors[name] = false;
      finalCallback();
      return;
    }

    $.each(this.options.rules[name], function (ruleName, rule) {
      if (!validators.exist(ruleName)) {
        return;
      }

      var hasDependenceCondition = _.isFunction(rule['depends']), // do we have any precondition?
        dependenceConditionValue = hasDependenceCondition ? rule['depends'](input) : false, // execute it if it is
        params = rule['params'] || rule,
        value = input.value,
        result;

      if (hasDependenceCondition && !dependenceConditionValue) { // in this case we don't have to validate this field, i.e. if validate mobile we see that email is checked
        that.successCallback(name, withoutRender);
      } else if (!hasDependenceCondition || dependenceConditionValue) {
        result = validators.applyValidator(that, ruleName, value, params, input, function (result) {
          that.resultCallback(result, ruleName, name, withoutRender);
        });

        if (result === null) {
          return; // continue the cycle
        }

        if (result.state() == 'rejected') { // if we have already rejected this promise we should break the chain
          deferreds = [];
          return false; // break the cycle
        }
        deferreds.push(result)
      }
    });

// if we have any async calls then wait for all them to be done

    if (deferreds.length) {
      $.when.apply(null, deferreds) // wait for resolving all deferreds
        .always(finalCallback); // always call final callback
      return deferreds;
    }

    finalCallback()
  };

  FormValidator.prototype.getFinalValidationCallback = function (withoutRender) {
    return $.proxy(function () {
      if (!withoutRender) this.tryChangeSubmitButtonClass();
    }, this);
  };

  FormValidator.prototype.successCallback = function (name, withoutRender) {
    this.errors[name] = '';
    if (!withoutRender) this.renderSuccess(name);
  };

  FormValidator.prototype.errorCallback = function (ruleName, name, withoutRender) {
    var messages = this.options.messages[name],
      errorMessage = !_.isUndefined(messages) && !_.isUndefined(messages[ruleName]) ? messages[ruleName] : this.defaultMessage;

    this.errors[name] = _.isFunction(errorMessage) ? errorMessage() : errorMessage;
    if (!withoutRender) this.renderError(name);
  };

  FormValidator.prototype.resultCallback = function (result, ruleName, name, withoutRender) {
    if (!result) {
      this.errorCallback(ruleName, name, withoutRender)
    } else {
      this.successCallback(name, withoutRender)
    }
  };

  FormValidator.prototype.renderError = function (fieldName) {
    if (!_.isFunction(this.options.errorPlacement)) {
      return;
    }
    this.options.errorPlacement(fieldName, this.errors[fieldName]);
  };

  FormValidator.prototype.renderSuccess = function (fieldName) {
    if (!_.isFunction(this.options.successPlacement)) {
      return;
    }
    this.options.successPlacement(fieldName);
  };

  FormValidator.prototype.tryChangeSubmitButtonClass = function () {
    if (this.options.changeSubmitButtonClass) {
      var submitButton = this.form.find('input[type="submit"]'),
        isValid = this.isFormValid();

      if (!isValid) {
        submitButton.addClass('disabled');
      } else {
        submitButton.removeClass('disabled');
      }
    }
  };

  FormValidator.prototype.isFormValid = function () {
    var result = true;
    $.each(this.errors, function (fieldName, error) {
      if (!!error) {
        result = false;
        return false;
      }
    });
    return result;
  };


  FormValidator.prototype.bindSubmitHandler = function () {
    var that = this,
      isRunning = false;

    this.form.on('submit', function () {

      if (!that.options.submitHandler && that.isValid) { // if it's true then we called it from this function having set flag to true
        return true
      }
// prevent parallel submiting
      if (isRunning) {
        return false;
      }
      isRunning = true;

      that.runValidation()
        .done(function () {
          var result = true;
          if (that.options.additionalValidationHandler) {
            result = that.options.additionalValidationHandler(that.form);
          }
          isRunning = false;
          if (that.isValid = that.isFormValid() && result) {
            that.options.submitHandler ? that.options.submitHandler.call(that, that.form) : that.form.get(0).submit();
          }
        });

      return false;
    });
  };

  var validators = {
    exist: function (name) {
      return _.isFunction(this.list[name]);
    },
    applyValidator: function (context, name) {
      var args = Array.prototype.slice.call(arguments, 2),// slice the args, the first is name parameter
        callback = args.pop(), // the last argument is callback
        that = this,
        result,
        deferred = $.Deferred();

      if (!this.exist(name)) {
        return null;
      }

      deferred.always(function (result) {
        callback.call(that, result);
      });

      args.push(deferred);

      result = this.list[name].apply(context, args);
      if (!this.isAsync(name)) {
        result ? deferred.resolve(result) : deferred.reject(result);
      }
      return deferred.promise();
    },
    isAsync: function (name) {
      return name == 'remote';
    },
    list: {
      required: function (value, expected) {
        if (typeof expected == 'function') {
          expected = expected.call(this)
        }
        return expected ? $.trim(value).length > 0 : true;
      },
      checked: function (value, param, elem) {
        return $(elem).is(':checked');
      },
      minlength:

        function (value, param) {
          return $.trim(value).length >= param;
        },
      maxlength: function (value, param) {
        return $.trim(value).length <= param;
      },
      email: function (value) {
        value = $.trim(value);
        return !value.length || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(value);
      },
      url: function (value) {
        value = $.trim(value);
        return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
      },
      date: function (value) {
        value = $.trim(value);
        return !/Invalid|NaN/.test(new Date(value).toString());
      },
      dateISO: function (value) {
        value = $.trim(value);
        return /^\d{4}[\/\-]\d{1,2}[\/\-]\d{1,2}$/.test(value);
      },
      number: function (value) {
        value = $.trim(value);
        return /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
      },
      digits: function (value) {
        value = $.trim(value);
        return /^\d+$/.test(value);
      },
      oneDigit: function (value) {
        value = $.trim(value);
        return /\d{1,}/.test(value);
      },
// based on http://en.wikipedia.org/wiki/Luhn
      creditcard: function (value) {
        value = $.trim(value);
        if (/[^0-9 \-]+/.test(value)) {
          return false;
        }
        var nCheck = 0,
          nDigit = 0,
          bEven = false;

        value = value.replace(/\D/g, "");

        for (var n = value.length - 1; n >= 0; n--) {
          var cDigit = value.charAt(n);
          nDigit = parseInt(cDigit, 10);
          if (bEven) {
            if ((nDigit *= 2) > 9) {
              nDigit -= 9;
            }
          }
          nCheck += nDigit;
          bEven = !bEven;
        }

        return (nCheck % 10) === 0;
      },
      min: function (value, param) {
        value = $.trim(value);
        return value >= param;
      },
      max: function (value, param) {
        value = $.trim(value);
        return value <= param;
      },
      equalTo: function (value, param) {
        value = $.trim(value);
        var target = $(param);
        return value === target.val();
      },
      mobileNoMask: function(value) {
        value = $.trim(value);
        return !value.length || /^\d{10}$/i.test(value);
      },
      remote: function (value, param, elem, deferred) {
        value = $.trim(value);
        $.ajax({
          url: param,
          type: 'POST',
          dataType: "json",
          data:
            {
              data: value
            }
          ,
          success: function (response) {
            if (!response) {
              deferred.resolve(true);
              return;
            }
            var result = response['status'] === true || response['status'] === "true" || response['status'] === "ok";
            deferred.resolve(result);
          }
        })
        ;
      },
      regexp: function (value, param) {
        value = $.trim(value);
        return param.test(value);
      },
      nospecialchars: function (value) {
        value = $.trim(value);
        return !value.length || !/[^@_\.\-1234567890qwertyuiopasdfghjklzxcvbnm ЁЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮёйцукенгшщзхъфывапролджэячсмитьбюAĀBCČDEĒFGĢHIĪJKĶLĻMNŅOPRSŠTUŪVZŽaābcčdeēfgģhiījkķlļmnņoprsštuūvzž]/i.test(value);
      },
      nospecialcharsNocyrillic: function (value) {
        value = $.trim(value);
        return !/[^@_\.\-1234567890qwertyuiopasdfghjklzxcvbnm]/i.test(value);
      },
      custom: function (value, param) {
        value = $.trim(value);
        return typeof param == 'function' && param(value);
      },
      nocyrillic: function (value) {
        value = $.trim(value);
        return !/[а-яёА-ЯЁ]/.test(value);
      },
      nonumbers: function (value) {
        value = $.trim(value);
        return !/\d{1,}/.test(value);
      },
      nochars: function (value) {
        value = $.trim(value);
        return !value.length || /^[\d\s]+$/.test(value);
      },
      nospaces: function (value) {
        return (value.indexOf(' ') === -1)
      },
      mobile: function (value) {
        value = $.trim(value);
        return !value.length || /^\(\d{3}\)\-\d{3}-\d{2}-\d{2}$/i.test(value);
      }
    }
  };

  return FormValidator;
})();
