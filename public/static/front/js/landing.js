jQuery('document').ready(function () {
    var sceneMembership = $('.section_membership .section__scene').get(0);
    var membershipInstance = new Parallax(sceneMembership);

    var sceneOff = $('.section_off .section__scene').get(0);
    var offInstance = new Parallax(sceneOff);


    $('.map-block__towns').owlCarousel({
        items: 5,
        responsive: {
            0: {
                items: 1.2,
                center: false
            },
            768: {
                items: 5
            },
        },
        center: true,
        loop: true,
        margin: 20,
        nav: true,
        dots: false
    });

    var $mSlider = $('.members__list');
    var $mCount = $('.members__count');

    $mSlider.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
        var i = (currentSlide ? currentSlide : 0) + 1;
        $mCount.text(i + ' / ' + slick.slideCount);
    });

    $mSlider.slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: false,
        dots: false
    });
    $('.members__arrow_prev').on('click', function(e){
        e.preventDefault();
        $('.members__list .slick-prev').click();
    });
    $('.members__arrow_next').on('click', function(e){
        e.preventDefault();
        $('.members__list .slick-next').click();
    });

    $('.membership__scroll').on('click', function (e) {
        e.preventDefault();
        $([document.documentElement, document.body]).animate({
            scrollTop: $('.section_about').offset().top
        })
    });

    var $save_value = $('.total__savings-value');
    var $nonmember_value = $('.total__nonmembers-value');
    var $member_value = $('.total__members-value');
    // var values = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    // var values_p = ["0 Months", "1 Months", "2 Months", "3 Months", "4 Months", "5 Months", "6 Months", "7 Months", "8 Months", "9 Months", "10 Months", "10 Months", "12+ Months"];
    var priceSlider = document.getElementById('price-slider');

    $calcInput = $('.calculator__input');

    var orientationStr = 'horizontal';

    if ($(window).width() < 768) {
        orientationStr = 'vertical';
    }
    ;

    noUiSlider.create(priceSlider, {
        start: [0],
        connect: true,
        step: 1,
        orientation: orientationStr,
        tooltips: [wNumb({decimals: 0, suffix: ' Months'})],
        range: {
            'min': 0,
            'max': 12
        }
    });
    priceSlider.noUiSlider.on('update', function (values, handle) {
        $calcInput.val(Number(values[0]).toFixed(0));
        checkSlider();
        // if (handle) {
        //     marginMax.innerHTML = values[handle];
        // } else {
        //     marginMin.innerHTML = values[handle];
        // }
    });
    // $(".calculator__input").ionRangeSlider({
    //     min: 0,
    //     max: 12,
    //     from: 0,
    //     to: 12,
    //     max_postfix: "+",
    //     postfix: " Months",
    //     onChange: function(data) {
    //         checkSlider();
    //     }
    // });
    $('.room-el__item ').on('click', function (e) {
        e.preventDefault();
        var t = $(this);
        if (!t.hasClass('room-el__item_selected')) {
            $('.room-el__item_selected').removeClass('room-el__item_selected');
            t.addClass('room-el__item_selected');
            checkRoom();
            $('.room-el').addClass('room-el_opacity');
        }
        ;
    });
    $('.room-el__address-text').on('click', function (e) {
        e.stopPropagation();
    });

    function checkRoom() {
        if ($('.room-el__item_selected').length > 0) {
            $('.calculator__slider-wrapper').slideDown({
                start: function () {
                    $(this).css({
                        display: "flex"
                    })
                }
            });
            calculate($('.room-el__item_selected').attr('data-price'), $('.calculator__input').val());
        } else {
            $('.calculator__slider-wrapper').removeClass('calculator__slider-wrapper_active');
        }
        ;
    };

    function checkSlider() {
        var inp = $('.calculator__input');
        var $total = $('.total');
        var $addons = $('.addons')
        if (inp.val() > 0) {
            $total.slideDown();
            $addons.slideDown();
            calculate($('.room-el__item_selected').attr('data-price'), inp.val());
        } else {
            $total.slideUp();
            $addons.slideUp();
        }
        ;
        $('.total__savings-label-value').text(inp.val());
    };

    function calculate(price, month) {
        var price_member;
        var total_price;
        var month_member = price - 300;
        var save = 300;
        if (month > 0) {
            switch (month) {
                case '6':
                case '7':
                case '8':
                case '9':
                case '10':
                case '11':
                    save = 200;
                    month_member = price - 200;
                    total_price = price - (price / 100 * 7.5);
                    price_member = month_member - (month_member / 100 * 7.5);
                    break;
                case '12':
                    save = 150;
                    month_member = price - 150;
                    total_price = price - (price / 100 * 15);
                    price_member = month_member - (month_member / 100 * 15);
                    break;
                default:
                    total_price = price;
                    price_member = month_member;
            }
            $nonmember_value.text('$' + (Number(total_price).toFixed(0)));
            $member_value.text('$' + ((price_member) + getAddons()).toFixed(0));
            $save_value.text('$' + (save * month));
        }
        ;
    }

    function getAddons() {
        var addons_sum = 0;
        $('.addons__input:checked').each(function () {
            addons_sum += Number($(this).val());
        });
        return Number(addons_sum);
    };

    $('.addons__input').on('change', function () {
        calculate($('.room-el__item_selected').attr('data-price'), $('.calculator__input').val());
    });


    var controller = new ScrollMagic.Controller();

    var els = [
        {sel: '.membership__logo', val: 'puff-in-center'},
        {sel: '.membership__slogan', val: 'fade-in-bottom'},
        {sel: '.about__man', val: 'fade-in-left'},
        {sel: '.about__content', val: 'fade-in__delay'},
        {sel: '.included', val: 'fade-in'},
        {sel: '.experience', val: 'fade-in'},
        {sel: '.monthly-price__content', val: 'fade-in-left'},
        {sel: '.membership__slogan', val: 'fade-in-bottom'},
        {sel: '.monthly-price__img', val: 'fade-in'},
        {sel: '.map-block__towns', val: 'fade-in__delay'},
        {sel: '.map-block__wrapper', val: 'fade-in'},
        {sel: '.members', val: 'fade-in'},
        {sel: '.weekend__logo', val: 'puff-in-center'},
        {sel: '.weekend__content', val: 'fade-in-bottom'},
        {sel: '.calculator', val: 'fade-in'},
        {sel: '.become', val: 'fade-in'},
        {sel: '.homes', val: 'fade-in-bottom'}

        ];

    els.forEach(function(item, i, arr) {
        new ScrollMagic.Scene({triggerElement: $(item.sel).get(0)}).setClassToggle($(item.sel).get(0), item.val).reverse(false).addTo(controller);
    });

});