$('document').ready(function () {

    var $houses = $('.houses');

    var controller = new ScrollMagic.Controller();

    var els = [
        {sel: '.houses__item', val: 'fade-in'}
    ];

    new ScrollMagic.Scene({
        triggerElement: $('.header').get(0)
    }).setClassToggle($('.header').get(0), 'fade-in__delay').reverse(false).addTo(controller);

    $.getJSON( "https://junehomes.com/api/v1/residences/list/?with_tour=True", function( data ) {
        // var items = [];
        $.each( data, function( key, val ) {
            // console.log(cl);
            $houses.append('<a data-fancybox data-type="iframe" href="javascript:;" data-src="' + val.tour_url + '" class="houses__item" style="background-image: url(' + val.picture_url + ');">\n' +
                '            <div class="houses__inner"><div class="houses__virtual">TAP TO START Virtual Walk</div>\n' +
                '              <div class="houses__town"><span class="houses__town-inner">' + val.city_title + '</span></div>\n' +
                '              <div class="houses__title">' + val.title + '</div>\n' +
                '              <div class="houses__text">' + val.description + '</div>\n' +
                '              <div class="houses__price">From $' + val.min_month_price + '/month</div>\n' +
                '            </div>\n' +
                '          </a>');
            // items.push( "<li id='" + key + "'>" + val.title + "</li>" );
        });



        $('[data-fancybox]').fancybox({
            toolbar  : false,
            smallBtn : true,
            iframe : {
                preload : false
            }
        });

        $('.houses__loading').remove();



        // $( "<ul/>", {
        //     "class": "my-new-list",
        //     html: items.join( "" )
        // }).appendTo( ".houses" );
        $('.houses__text').clamp({clamp:2});


        els.forEach(function(item, i, arr) {
            $(item.sel).each(function(){
                var cur = $(this);
                new ScrollMagic.Scene({
                    triggerElement: cur.get(0),
                    offset: -180
                }).setClassToggle(cur.get(0), item.val).reverse(false).addTo(controller);
            });
        });
    });




});